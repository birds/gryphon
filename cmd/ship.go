package cmd

import (
	"github.com/spf13/cobra"
)

// shipCmd represents the ship command
var shipCmd = &cobra.Command{
	Use:   "ship",
	Short: "Interact with a ship simulation",
	Long: ``,
}

func init() {
	RootCmd.AddCommand(shipCmd)
}
